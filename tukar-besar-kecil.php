<?php
function tukar_besar_kecil($string){
    //kode di sini
    $hasil = '';
    for($i = 0; $i < strlen($string); $i++){

        $karakter = $string[$i];
        $ascii = ord(strtoupper($karakter));
        if($karakter != ' ' && ( $ascii >= 60 && $ascii <= 90)){
            if($string[$i] == strtoupper($string[$i]) ){
                $karakter = chr(ord($string[$i]) + 32);
            }else{
                $karakter = chr(ord($string[$i]) - 32);
            }
            
        }
        $hasil .= $karakter;
        
    }

    return $hasil;
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo "<br>";
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo "<br>";
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo "<br>";
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo "<br>";
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>