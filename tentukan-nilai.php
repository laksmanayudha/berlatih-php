<?php
function tentukan_nilai($number)
{
    //  kode disini
    $string = '';
    if ($number >= 85 && $number <= 100){
        $string .= 'Sangat Baik';
    }else if($number >= 70 && $number < 85){
        $string .= 'Baik';
    }else if($number >= 60 && $number < 70){
        $string .= 'Cukup';
    }else{
        $string .= 'Kurang';
    }
    return $string;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";
echo tentukan_nilai(76); //Baik
echo "<br>";
echo tentukan_nilai(67); //Cukup
echo "<br>";
echo tentukan_nilai(43); //Kurang
?>